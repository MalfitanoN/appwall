"use strict";
Template.form.events({
    'submit .add-new-task': function (event) {
        event.preventDefault();
        var postImage = event.currentTarget.children[0].children[0].children[1].files[0];
        var postName = event.currentTarget.children[2].value;
        var postMessage = event.currentTarget.children[4].value;

        if (postName === "") {
            Materialize.toast('NO TEXT...', 4000);
            return false;
        }

        Collections.Images.insert(postImage, function (error, fileObject) {
            if (error) {
                Materialize.toast('Beans', 4000);
                return false;
            } else {
                Materialize.toast('Posting...', 4000);
                Collections.Tasks.insert({
                    name: postName,
                    createdAt: new Date(),
                    Message: postMessage,
                    imageId: fileObject._id
                });
                $('.grid').masonry('reloadItems');
            }
        });
    }
});
 
  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });

